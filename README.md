json for PostgreSQL 9.1
=======================

To build it as an extension (but see below for binary upgradability), 
just do this:

    make
    make installcheck
    make install

If you encounter an error such as:

    "Makefile", line 8: Need an operator

You need to use GNU make, which may well be installed on your system as
`gmake`:

    gmake
    gmake install
    gmake installcheck

If you encounter an error such as:

    make: pg_config: Command not found

Be sure that you have `pg_config` installed and in your path. If you used a
package management system such as RPM to install PostgreSQL, be sure that the
`-devel` package is also installed. If necessary tell the build process where
to find it:

    env PG_CONFIG=/path/to/pg_config make && make installcheck && make install

If you encounter an error such as:

    ERROR:  must be owner of database regression

You need to run the test suite using a super user, such as the default
"postgres" super user:

    make installcheck PGUSER=postgres

Once json is installed, you can add it to a database. Just run:

    CREATE EXTENSION json;

Or possibly:

   CREATE EXTENSION json WITH SCHEMA pg_catalog;

Binary Upgrade-ability
----------------------

To install for binary upgrade-ability, you need to do things a bit differently.
The reason is that in 9.2 and later this is NOT an extension, and we don't
want pg_upgrade trying to install an unnecessary extension.

First, you need to build with a different makefile, and install NOT as an
extension:

    make -f Makefile.noext
    make -f Makefile.noext install

The to install in a database, do:

    psql -f /path/to/sharedir/contrib/json_noext.sql yourdb

This uses some magic imported from the pg_upgrade utility to make the json type
and its array type use the same type oids as are used in Release 9.2 so that
pg_upgrade will "just work" (almost). To do that it loads some routines
in a library called json_binup.so. After you have installed json, if you are
worried about superusers being able to load this library you can remove it - it
is only needed while you are installing json into a database.

When it comes time to upgrade the database to a new release, there is one more
thing you need to do. pg_upgrade will see that the catalog uses a library called
"$libdir/json" and if it can't load it will come screeching to a halt. It's
not needed, and pg_upgrade doesn't do anything with it. So you'll need to fool
pg_upgrade in your 9.2 or later installation by building and installing such a
module. There is one provided in the upgrade_dummy_module directory.

Change to that directory, and with your PATH pointing to the new installation
do
        make
        make install

After the upgrade, in the same directory and with the same path, do

        make uninstall

Dependencies
------------
The `json` data type has no dependencies other than PostgreSQL.

Copyright and License
---------------------

Copyright (c) 2012 The maintainer's name.

