/*
 * json.c  
 *
 */

#include "postgres.h"
#include "fmgr.h"

PG_MODULE_MAGIC;

/*
 * this is a dummy module whose only purpose is to fool
 * pg_upgrade which thinks that the old json module we
 * used in the 9.1 backport is required. It isn't, but
 * pg_upgrade fails without it.
 *
*/

