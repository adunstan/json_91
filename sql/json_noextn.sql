CREATE SCHEMA binoid;

CREATE FUNCTION binoid.set_next_pg_type_oid(OID)
   RETURNS VOID
   AS '$libdir/json_binup'
   LANGUAGE C STRICT;

CREATE FUNCTION binoid.set_next_array_pg_type_oid(OID)
   RETURNS VOID
   AS '$libdir/json_binup'
   LANGUAGE C STRICT;


SELECT binoid.set_next_pg_type_oid(114);
SELECT binoid.set_next_array_pg_type_oid(199);

SET search_path = 'pg_catalog';

CREATE TYPE json;

CREATE FUNCTION json_in(cstring)
RETURNS json
AS '$libdir/json'
LANGUAGE C STRICT IMMUTABLE;

CREATE FUNCTION json_out(json)
RETURNS cstring
AS '$libdir/json'
LANGUAGE C STRICT IMMUTABLE;

CREATE FUNCTION json_recv(internal)
RETURNS json
AS '$libdir/json'
LANGUAGE C STRICT IMMUTABLE;

CREATE FUNCTION json_send(json)
RETURNS bytea
AS '$libdir/json'
LANGUAGE C STRICT IMMUTABLE;

CREATE TYPE json (
        INTERNALLENGTH = -1,
        INPUT = json_in,
        OUTPUT = json_out,
        RECEIVE = json_recv,
        SEND = json_send,
        STORAGE = extended
);

CREATE FUNCTION array_to_json(anyarray)
RETURNS json
AS '$libdir/json','array_to_json'
LANGUAGE C STRICT IMMUTABLE;

CREATE FUNCTION array_to_json(anyarray, bool)
RETURNS json
AS '$libdir/json','array_to_json_pretty'
LANGUAGE C STRICT IMMUTABLE;

CREATE FUNCTION row_to_json(record)
RETURNS json
AS '$libdir/json','row_to_json'
LANGUAGE C STRICT IMMUTABLE;

CREATE FUNCTION row_to_json(record, bool)
RETURNS json
AS '$libdir/json','row_to_json_pretty'
LANGUAGE C STRICT IMMUTABLE;

DROP SCHEMA binoid CASCADE;



